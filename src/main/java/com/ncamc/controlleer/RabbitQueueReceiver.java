package com.ncamc.controlleer;

import com.alibaba.fastjson.JSONObject;
import com.rabbitmq.client.Channel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.listener.adapter.AbstractAdaptableMessageListener;
import org.springframework.stereotype.Component;

import java.nio.charset.StandardCharsets;

/**
 * 消费者
 */
@Slf4j
@Component
@RabbitListener(queues = {"rollback_queue"})
public class RabbitQueueReceiver extends AbstractAdaptableMessageListener {
 
    /**
     * 消息确认机制，消息不会重复消费
     * */
    @Override
    public void onMessage(Message message, Channel channel) throws Exception {
        //消息的唯一ID，单调递增正整数，从1开始，当multiple=trues，一次性处理<=deliveryTag的所有
        long deliveryTag = message.getMessageProperties().getDeliveryTag();
        boolean multiple=false;  //false单条   true 批量

        try {
            String msg=new String(message.getBody(), StandardCharsets.UTF_8);
            JSONObject json = JSONObject.parseObject(msg);
            Long id = json.getLong("id");
            log.info("消费的消息id_"+id+"-------->>>>>>>"+"消费者消费消息的消息体：{}----->>>>>"+message);
 
            //睡眠四秒
            for(int i=0;i<4;i++){
                Thread.sleep(1000);
                System.out.println("...");
            }
 
            log.info("消息已被正确消费--->>>>>>>>"+deliveryTag);
            //当前模式为单条消费
            channel.basicAck(deliveryTag, multiple);
 
        } catch (Exception e) {
            e.printStackTrace();
            //报异常重新投递
            channel.basicReject(deliveryTag,true);
        }
    }
}